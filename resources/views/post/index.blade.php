@extends ('adminlte.master')

@section('content')
<div class="ml-3 mt-3">

<div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Posts Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
              <div class="alert alert-success">
              {{session('success')}}
              </div>
              @endif
              <a class="btn btn-primmary" herf="/post/create">Create New Post"</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Title</th>
                      <th>Body</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse(posts as $key => $post)
                    <tr>
                        <td> {{$key + 1}}</td>
                        <td> {{$post + title}}</td>
                        <td> {{$post + body}}</td>
                        <td> 
                        <a herf="#" class="btn btn-info btn-sm">show </a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                    <td colspan="4" align="center"> No Posts</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
  

            <!-- /.card -->
          </div>

</div>
@endsection