@extends ('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="card card-primary">
             <div class="card-header">
                <h3 class="card-title">Edit Post{{$post->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/posts" method="POST">
              @csrf 
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ old('title',$post->title )}}" placeholder="Enter title" required>
                    @error('title')
                      <div class ="alret alret-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="body">Body</label>
                    <input type="text" class="form-control" id="body" name="body" value="{{ old('body',$post->body )}}" placeholder="Password" required>
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
          </div>
@endsection