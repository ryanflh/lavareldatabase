<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return "aman";
});

Route::get('master', function () {
    return view('adminlte.master');
});

Route:: get('/posts/create','PostController@create');
Route:: get('/posts','PostController@create');
Route:: get('/posts','PostController@index');
Route:: get('/posts/{id}','PostController@show');
Route:: get('/posts/{id}/edit','PostController@edit');
Route:: get('/posts/{id}','PostController@update');
Route:: get('/posts/{id}','PostController@destroy');