<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\DB;
//use Illuminate\Validation\Rules\Unique;
//use Symfony\Contracts\Service\Attribute\Required;
use DB;

class PostController extends Controller
{
    public function create(){

        return view ('posts.create');
    }

    public function store(Request $request ){
        //dd($request->all());

        $request->validate([
            'title'=>'Required|Unique:post',
            'body' => 'required'
        ]);

        $query = DB::table('post')->insert([
            "title" => $request["table"],
            "body" => $request["body"]

        ]);
        return redirect('/post')->with('sucess','Berhasil disimpan');
    }

    public function index(){
        $posts = DB:: table ('post')->get();
        //dd($posts);
        return view('post.index',compact('posts'));
    }

    public function show($id){
        $posts = DB:: table ('post')->where('id, $id')->first();
        //dd($posts)
        return view('post.show',compact('posts'));
    }
    public function edit($id){
        $posts = DB:: table ('post')->where('id, $id')->first();
        //dd($posts)
        return view('post.edit',compact('posts'));
    }

    public function update($id, Request $request){
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required'
        ]); 

        $query = DB::table('posts')
            ->where('id',$id)
            ->update([
                'tiitle' =>  $request['title'],
                'body' => $request['body']
            ]); return redirect('/posts')->with('sucess', 'Berhasil update Post'); 
    }
    public function destroy($id){
        $query = DB:: table ('post')->where('id, $id')->delete();
        return redirect('/posts')->with('sucess','post telah dihapus');
    }

    }

